import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

public class Main {

    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        PluginManager manager = new PluginManager("plugins");
        Plugin p;
        Plugin plugin = manager.load("FirstPlugin.jar", "Person");
        Plugin plugin2 = manager.load("SecondPlugin.jar", "Person");
        plugin.doUsefull();
        plugin2.doUsefull();


    }

}

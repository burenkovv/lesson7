import java.io.Closeable;
import java.net.URL;
import java.net.URLClassLoader;

public class NewClassLoader extends URLClassLoader implements Closeable {
    //тут взят просто конструктор из URLClassLoader
    public NewClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    public final Class<?> loadClass(String name, boolean resolve)
            throws ClassNotFoundException
    {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            int i = name.lastIndexOf('.');
            if (i != -1) {
                sm.checkPackageAccess(name.substring(0, i));
            }
        }
        //тут в URLClassLoader запускался super.loadClass(name, resolve);
        //я вместо него запускаю свой  скопипащеный и измененный loadClass
        return myLoadClass(name, resolve);
    }

    protected Class<?> myLoadClass(String name, boolean resolve)
            throws ClassNotFoundException
    {
        synchronized (getClassLoadingLock(name)) {
            Class<?> c = findLoadedClass(name);
            if (c == null) {
                //меняю местами так чтобы сначала была попытка самому загрузить класс, а уже если не выйдет,
                //то отправлялся бы запрос родителю
                try {
                    c = findClass(name);
                } catch (ClassNotFoundException e) {
                }
                if(c == null) {
                    try {
                        if (getParent() != null) {
                            return super.loadClass(name, resolve);
                        }
                    } catch (ClassNotFoundException e) {
                    }
                }
            }
            if (resolve) {
                resolveClass(c);
            }
            return c;
        }
    }
}

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginManager {
    private final String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public Plugin load(String pluginName, String pluginClassName) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        File file = new File(pluginRootDirectory + "\\"  + pluginName);
        URL url = file.toURI().toURL();
        URL[] urls = new URL[]{url};

        //Если вот этот кусок кода запускать, то работает до тех по, пока не создан класс Person в моём проекте

        URLClassLoader urlClassLoader = new URLClassLoader(urls, ClassLoader.getSystemClassLoader());
        Class<?> Class = urlClassLoader.loadClass(pluginClassName);
        System.out.println(urlClassLoader.getParent() == Plugin.class.getClassLoader());
        Plugin res = (Plugin) Class.newInstance();




        //а если этот, то не кастится.
        /*
        NewClassLoader urlClassLoader = new NewClassLoader(urls, ClassLoader.getSystemClassLoader());
        Class<?> Class = urlClassLoader.loadClass(pluginClassName);
        System.out.println(urlClassLoader.getParent() == Plugin.class.getClassLoader());
        Plugin res = (Plugin) Class.newInstance();
        */
        return res;

    }
}

import java.math.BigDecimal;
import java.util.Objects;
public class Person implements Plugin{
    private String name;
    private String surname;
    private String patronymic;
    private String cardNumber;


    public Person(){
    }

    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                '}';
    }

    public void doUsefull(){
        this.name = "Vasiliy";
        this.surname = "Vasiliev";
        this.patronymic = "Vasilievich";
        this.cardNumber = "7777 6666 7777 6666";
        System.out.println(this);
    }

}
